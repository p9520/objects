package auto;

public class Auto {
    private Double maxSpeed;//Поле, с максимальной скоростью

    //Конструктор класса, в качестве аргумента - максимальная скорость
    public Auto(Double speed) {
        maxSpeed = speed;
    }

    //Метод для получения максимальной скорости. Такие методы называются - Геттер.
    public Double getMaxSpeed() {
        return maxSpeed;
    }
}
