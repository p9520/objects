package seasons;

public enum Season {

    //Четыре обхекта перечисления
    WINTER("Зима", 90.25d),
    SPRING("Весна", 91d),
    SUMMER("Лето", 92d),
    AUTUMN("Осень", 91d);

    Season(String name, Double length) {
        this.name = name;
        this.length = length;
    }

    //Поля перечисления
    private String name;

    private Double length;

    public String getName() {
        return name;
    }

    public Double getLength() {
        return length;
    }
}
