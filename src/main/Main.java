package main;

import auto.Auto;
import plain.Plain;

public class Main {
    public static void main(String[] args) {

        Auto myBestAuto = new Auto(170d);
        System.out.println(myBestAuto.getMaxSpeed());

        Plain mySecondBestPlain = new Plain();
        System.out.println(mySecondBestPlain.getMark());
    }
}
